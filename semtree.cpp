#include "semtree.h"
#include <cassert>
#include <iostream>

using namespace std;

Node::Node()
{
    left = right = parent = nullptr;
    //tree = nullptr;
    lex="";
    //dataVal.dataAsLL = 0;
    dataType=INT;         // rand value in this field in some cases
}

Node *Node::createLeft()
{
    assert(this->left == nullptr);

    this->left = new Node();
    this->left->parent = this;

    return this->left;
}

Node *Node::createRight()
{
    assert(this->right == nullptr);

    this->right = new Node();
    this->right->parent = this;

    return this->right;
}

Node::~Node()
{
    /*
    if (arrDim.size()==1) {
        if (this->dataType==INT)
            delete[] dataVal.arrayInt;

        else if (this->dataType==INT64)
            delete[] dataVal.arrayLL;

        else assert(1==0);
    }
    */
}

SemTree::SemTree()
{
    root = new Node();
    root->dataType = INT;
    //root->dataVal.dataAsInt = 0;
    root->lex = "root";
    cur = root;

    root->parent = root;
}

Node* SemTree::getRoot()
{
    return this->root;
}

Node* SemTree::getCur()
{
    return this->cur;
}

void SemTree::setCur(Node *newCur)
{
    this->cur = newCur;
}

Node* SemTree::findSameLevel(Node *from, const std::string &lex)
{
    while (from == from->parent->left) {
        if (lex == from->lex)
            return from;

        from = from->parent;
    }

    return nullptr;
}

bool SemTree::isDuplicate(Node *from, const std::string &lex)
{
    if (findSameLevel(from, lex) == nullptr)
        return false;
    return true;

    return findSameLevel(from, lex) != nullptr;
}

Node* SemTree::semInclude(const string &lex, DATA_TYPE dt)
{
    if (isDuplicate(cur, lex) == true) {
        print_error("redifinition", lex);
    }

    if (dt == DATA_TYPE::FUNC) {
        cur = cur->createLeft();
        cur->lex = lex;                     // name of the function
        cur->dataType = DATA_TYPE::FUNC;

        cur = cur->createRight();
        return cur->parent;
    }
    else {          // dt == INT, INT64
        cur = cur->createLeft();
        cur->lex = lex;                     // name of the variable or array
        cur->dataType = dt;
        return cur;
    }

    cerr << "never be here";
}

void SemTree::CompoundOpCreateScope()
{
    cur = cur->createLeft();        // Is it needed? May be create right only?
    cur = cur->createRight();
}

void SemTree::LeaveOneScope()
{
    while (cur->parent->left == cur)
        cur = cur->parent;
    cur = cur->parent;
}

void SemTree::semAddArrDimansion(int dim)
{
    cur->arrDim.push_back(dim);
}

Node *SemTree::findUp(const string &str)
{
    Node *node = cur;
    while (node->parent != node) {
        if (str == node->lex)
            return node;

        node = node->parent;
    }

    return nullptr;
}

void SemTree::print_error(const string &m1, const string &m2) const
{
    scanner2printErrors->print_error(m1, m2);
}

void SemTree::printTree()
{
    _printTree(root, 0);
}

void SemTree::_printTree(Node *n, int indent)
{
    if (n == nullptr) return;

    cerr << n->lex;

    if (n->dataType == DATA_TYPE::FUNC)
        cerr << " function at idx " << n->func_triad_idx << endl;

    else if (n->arrDim.size() == 1) {
        if (n->dataType==INT)
            cerr << " INTarray of dim " << n->arrDim[0] << endl;
        else if (n->dataType==INT64)
            cerr << " LLarray of dim " << n->arrDim[0] << endl;
        else assert(1==0);
    }
    else {
        if (n->dataType==INT) cerr << " INTvar" << endl;
        else if (n->dataType==INT64) cerr << " LLvar" << endl;
        else assert(1==0);
    }

    _printTree(n->left, indent);
    _printTree(n->right, indent+1);
}

void SemTree::deleteSubTree(Node *n)
{
    if (n == nullptr)
        return;

    deleteSubTree(n->left);
    deleteSubTree(n->right);

    delete n;
}
