#ifndef CODEGENERATOR_H
#define CODEGENERATOR_H
#include "triadgenerator.h"
#include <stack>

class Reg {
public:
    enum Name {
        notset = 0, eax, ebx, ecx, edx, esi, edi
    };

    Reg();
    Reg(const Reg &toCopy);
    Reg(Reg::Name nm);

    Name name;
    bool isBusy;

    string getName();
};

class CodeGenerator
{
private:
    vector<Triad> triads;
    SemTree *st;

    vector<Reg> allRegs;
    stack<Reg> regStack;
    vector<string> asmCode;

    int ebpOffset;
    void rec(Node *node);
    Reg giveFreeReg();
    void freeReg(Reg reg);
    Node *init();

    int toPush;
public:

    void generateAsm(vector<Triad> &triads, SemTree *st);
};

#endif // CODEGENERATOR_H
