#include "parser.h"
#include <iostream>
#include <climits>
#include <cassert>

#define TT TokenType

using namespace std;
using lexer::Token;
using lexer::TokenType;

Parser::Parser(lexer::Scanner *sc) : scanner(sc)
{
    flInt = true;
    st.scanner2printErrors = sc;
}

// Program is a sequance of data definitions and function definitions
void Parser::P()
{
    flInt = true;
    Token t = scanner->peekToken();

    while (t.type() == TokenType::TInt || t.type() == TokenType::TLL ||
           t.type() == TokenType::TVoid)
    {
        if (t.type() == TokenType::TInt || t.type() == TokenType::TLL) {
            D();
        }
        else {          // func declaration
            G();
        }

        t = scanner->peekToken();
    }

    if (t.type() == TT::TEnd){
        scanner->print_error("All text parsed.");
    }
    else {
        scanner->print_error("kernel panic, ", t);
    }
}

// Data definition (int a = 5;)
void Parser::D()
{
    Token t = scanner->nextToken();
    if (t.type() != TokenType::TInt && t.type() != TokenType::TLL) {
        scanner->print_error("int or __int64 D", t);
    }

    assert (t.type() == TokenType::TInt || t.type() == TokenType::TLL);
    DATA_TYPE dt = (t.type() == TT::TInt)? DATA_TYPE::INT : DATA_TYPE::INT64;

    Z(dt);

    t = scanner->nextToken();
    if (t.type() != TokenType::TSc)
        scanner->print_error("; D", t);
}

// Function definition
// void a(){...}
void Parser::G()
{
    Token t = scanner->nextToken();
    if (t.type() != TT::TVoid)
        scanner->print_error("void G", t);

    t = scanner->nextToken();
    if (t.type() != TT::TIdent)
        scanner->print_error("identifier G", t);

    Node *ret = st.semInclude(t.lexeme(), DATA_TYPE::FUNC);

    t = scanner->nextToken();
    if (t.type() != TT::TLParen)
        scanner->print_error("( G", t);

    t = scanner->nextToken();
    if (t.type() != TT::TRParen)
        scanner->print_error(") G", t);

    //X();inlined below
    t = scanner->nextToken();
    if (t.type() != TT::TLBrace)
        scanner->print_error("{ G", t);

    ret->dataVal.scannerPos = scanner->getPos();

    if (ret->lex != "main")
        flInt = false;
    else {
        1+1;
    }
    Q();
    flInt = true;

    t = scanner->nextToken();
    if (t.type() != TT::TRBrace)
        scanner->print_error("} G", t);

    st.setCur(ret);                 // exited function scope. sem
}

// stub method as only int32 and int64 exists.
bool semAssignmentTypeCheck(TData dt1, TData dt2)
{
    dt1 = dt1;
    dt1 = dt2;        // delme -warning unused param
    return true;
}

// Data definition sequance (a, a = 5, a[7] = 5)
void Parser::Z(DATA_TYPE dt)
{
    Token t;

    int pos;

    do {
        t = scanner->nextToken();
        if (t.type() != TT::TIdent)
            scanner->print_error("identifier Z", t);

        Node *newVar = nullptr;
        if (flInt)
            newVar = st.semInclude(t.lexeme(), dt);

        pos = scanner->getPos(); t = scanner->nextToken();

        if (t.type() == TT::TAssign) {

            TData dtV = V();
            if (flInt) {
                if (dt == INT)
                    newVar->dataVal.dataAsInt = ((dtV.dt==DATA_TYPE::INT)? dtV.val.dataAsInt : dtV.val.dataAsLL);
                else if (dt == INT64)
                    newVar->dataVal.dataAsLL = ((dtV.dt==DATA_TYPE::INT)? dtV.val.dataAsInt : dtV.val.dataAsLL);
            }

            pos = scanner->getPos(); t = scanner->nextToken();
        }
        else if (t.type() == TT::TLSqB) {
            t = scanner->nextToken();
            if (t.type() != TT::TDecLiter && t.type() != TT::THexLiter)
                scanner->print_error("number Z", t);

            if (flInt) {
                st.semAddArrDimansion(std::stoi(t.lexeme(), nullptr, 0));       // add one dimension to array
                if (newVar->dataType==INT)
                    newVar->dataVal.arrayInt = new int[newVar->arrDim[0]];
                else if (newVar->dataType==INT64)
                    newVar->dataVal.arrayLL = new long long[newVar->arrDim[0]];
            }

            t = scanner->nextToken();
            if (t.type() != TT::TRSqB)
                scanner->print_error("] Z", t);

            pos = scanner->getPos(); t = scanner->nextToken();
        }


    } while (t.type() == TT::TComma);

    scanner->setPos(pos);
}

/* inlined
// Compound operator {..}
void Parser::X()
{
    Token t;

    t = scanner->nextToken();
    if (t.type() != TT::TLBrace)
        scanner->print_error("{", t);

    Q();

    t = scanner->nextToken();
    if (t.type() != TT::TRBrace)
        scanner->print_error("}", t);
}
//*/

// sequance of operators and data definitions (in compound operator {...});
void Parser::Q()
{
    Token t = scanner->peekToken();
    TT type = t.type();

    // follow Q := '}'
    while (type == TT::TInt || type == TT::TLL || type == TT::TIdent || type == TT::TFor ||
           type == TT::TLBrace || type == TT::TDecLiter || type == TT::THexLiter ||
           type == TT::TMinus || type == TT::TLParen || type == TT::TSc)
    {
        if (t.type() == TT::TInt || t.type() == TT::TLL) {
            D();
        }
        else {
            O();
        }

        t = scanner->peekToken();
        type = t.type();
    }
}

// Operator. asignment, for, {...}.
void Parser::O()
{
    int pos = scanner->getPos();
    //std::cerr << "Reached O at pos " << pos << std::endl;
    Token t = scanner->nextToken();

    if (t.type() == TT::TIdent || t.type() == TT::TSc) {
        if (t.type() == TT::TIdent) {
            Node *node = nullptr;
            DATA_TYPE dt;

            if (flInt) {
                node = st.findUp(t.lexeme());

                if (node == nullptr)
                    scanner->print_error("Undefined variable", "");
                dt = node->dataType;
            }

            t = scanner->nextToken();
            if (t.type() == TT::TLParen) {                      // operator function call

                if (flInt) {
                    if (node->dataType != DATA_TYPE::FUNC)
                        scanner->print_error("Expected name of the function", "");

                    Node *save = st.getCur();                   // context save
                    int scannerPosSave = scanner->getPos();

                    Node *funNode = new Node();                 // context switch
                    *funNode = *node;
                    funNode->parent = node;
                    node->left = funNode;
                    Node *dummyRight = new Node();
                    funNode->right = dummyRight;
                    dummyRight->parent = funNode;
                    st.setCur(dummyRight);
                    scanner->setPos(funNode->dataVal.scannerPos);

                    Q();                                        // execute function body

                    st.deleteSubTree(dummyRight);
                    node->left = funNode->left;
                    delete funNode;

                    st.setCur(save);                            // context reset
                    scanner->setPos(scannerPosSave);
                }

                t = scanner->nextToken();
                if (t.type() != TT::TRParen)
                    scanner->print_error(") O", t);
            }
            else if (t.type() == TT::TAssign) {                 // assignment operator

                if (flInt) {
                    if (node->arrDim.size() > 0)
                        scanner->print_error("Array instead of variable", "");

                    if (node->dataType == DATA_TYPE::FUNC) {
                        scanner->print_error("function in assign operator", "");
                    }
                }

                TData Tval = V();

                if (flInt) {
                    if (node->dataType == INT)
                        node->dataVal.dataAsInt =
                                ((Tval.dt==DATA_TYPE::INT)? Tval.val.dataAsInt : Tval.val.dataAsLL);

                    else if (node->dataType == INT64)
                        node->dataVal.dataAsLL =
                                ((Tval.dt==DATA_TYPE::INT)? Tval.val.dataAsInt : Tval.val.dataAsLL);

                    else assert(1==0);
                }
            }
            else if (t.type() == TT::TLSqB) {
                if (flInt) {
                    if (node->arrDim.size() == 0)
                        scanner->print_error("Variable instead of array", "");
                }

                TData Tidx = V();                       // index in array

                t = scanner->nextToken();
                if (t.type() != TT::TRSqB)
                    scanner->print_error("] O", t);

                t = scanner->nextToken();
                if (t.type() != TT::TAssign)
                    scanner->print_error("= O", t);

                TData Tval = V();                       // value to store in array at index Tidx

                if (flInt) {
                    int idx = ((Tidx.dt==DATA_TYPE::INT)? Tidx.val.dataAsInt : Tidx.val.dataAsLL);
                    if (idx < 0 || idx >= node->arrDim[0])
                        scanner->print_error(node->lex + " array index out of range O", t);

                    if (node->dataType == INT)
                        node->dataVal.arrayInt[idx] =
                                ((Tval.dt==DATA_TYPE::INT)? Tval.val.dataAsInt : Tval.val.dataAsLL);

                    else if (node->dataType == INT64)
                        node->dataVal.arrayLL[idx] =
                                ((Tval.dt==DATA_TYPE::INT)? Tval.val.dataAsInt : Tval.val.dataAsLL);

                    else assert(1==0);

                }
            }

            t = scanner->nextToken();
            if (t.type() != TT::TSc)
                scanner->print_error("; O", t);
        }
    }
    else if (t.type() == TT::TFor) {
        scanner->setPos(pos);
        U();        // for
    }
    else if (t.type() == TT::TLBrace) {                     // compound operator
        Node *save = nullptr;
        Node *compBlock = nullptr;

        if (flInt) {
            save = st.getCur();
            compBlock = new Node();
            compBlock->parent = save;
            compBlock->left = save->left;
            if (compBlock->left != nullptr)
                compBlock->left->parent = compBlock;
            save->left = compBlock;

            compBlock->createRight();
            st.setCur(compBlock->right);
        }
        Q();

        if (flInt) {
            st.deleteSubTree(compBlock->right);
            save->left = compBlock->left;            // maybe save = compBlock.parent
            if (save->left != nullptr)
                save->left->parent = save;
            delete compBlock;

            st.setCur(save);
        }

        t = scanner->nextToken();
        if (t.type() != TT::TRBrace)
            scanner->print_error("} O", t);
    }
    else {
        scanner->print_error("something first O", t);
    }
}

DATA_TYPE semGetResType(DATA_TYPE dt1, DATA_TYPE dt2)
{
    if (dt1 == DATA_TYPE::INT64 || dt2 == DATA_TYPE::INT64)
        return DATA_TYPE::INT64;
    return DATA_TYPE::INT;
}

// this operations always return bool (INT in our case)
TData Parser::V()
{
    TData dt = M();

    Token t = scanner->peekToken();

    while (t.type() == TT::TEq || t.type() == TT::TNEq) {
        scanner->nextToken();

        TData dti = M();

        switch (t.type()) {
            case TT::TEq:
                dt.val.dataAsInt =
                ((dt.dt==DATA_TYPE::INT)?  dt.val.dataAsInt :  dt.val.dataAsLL)
                ==
                ((dti.dt==DATA_TYPE::INT)? dti.val.dataAsInt : dti.val.dataAsLL);
            break;

            case TT::TNEq:
                dt.val.dataAsInt =
                ((dt.dt==DATA_TYPE::INT)?  dt.val.dataAsInt :  dt.val.dataAsLL)
                !=
                ((dti.dt==DATA_TYPE::INT)? dti.val.dataAsInt : dti.val.dataAsLL);
            break;

            default:assert(1==0);
        }

        dt.dt = DATA_TYPE::INT;

        t = scanner->peekToken();
    }

    return dt;
}

// this operations always return bool (INT in our case)
TData Parser::M()
{
    TData dt = S();

    Token t = scanner->peekToken();

    while (t.type() == TT::TGT || t.type() == TT::TLT ||
           t.type() == TT::TGE || t.type() == TT::TLE) {
        scanner->nextToken();

        TData dti = S();        // not needed as this operation always returns BOOL (INT in our case).

        switch (t.type()) {
            case TT::TGT:
                dt.val.dataAsInt =
                ((dt.dt==DATA_TYPE::INT)?  dt.val.dataAsInt :  dt.val.dataAsLL)
                >
                ((dti.dt==DATA_TYPE::INT)? dti.val.dataAsInt : dti.val.dataAsLL);
            break;

            case TT::TGE:
                dt.val.dataAsInt =
                ((dt.dt==DATA_TYPE::INT)?  dt.val.dataAsInt :  dt.val.dataAsLL)
                >=
                ((dti.dt==DATA_TYPE::INT)? dti.val.dataAsInt : dti.val.dataAsLL);
            break;

            case TT::TLT:
                dt.val.dataAsInt =
                ((dt.dt==DATA_TYPE::INT)?  dt.val.dataAsInt :  dt.val.dataAsLL)
                <
                ((dti.dt==DATA_TYPE::INT)? dti.val.dataAsInt : dti.val.dataAsLL);
            break;

            case TT::TLE:
                dt.val.dataAsInt =
                ((dt.dt==DATA_TYPE::INT)?  dt.val.dataAsInt :  dt.val.dataAsLL)
                <=
                ((dti.dt==DATA_TYPE::INT)? dti.val.dataAsInt : dti.val.dataAsLL);
            break;

            default:assert(1==0);
        }

        dt.dt = DATA_TYPE::INT;

        t = scanner->peekToken();
    }

    return dt;
}

// just return type of the leftmost operand
TData Parser::S()
{
    TData dt = A();
    assert(dt.dt==INT || dt.dt==INT64);

    Token t = scanner->peekToken();

    while (t.type() == TT::TLSh || t.type() == TT::TRSh) {
        scanner->nextToken();
        TData dti = A();
        assert(dti.dt==INT || dti.dt==INT64);

        if (t.type() == TT::TLSh) {
            if (dt.dt==INT)
                dt.val.dataAsInt = dt.val.dataAsInt
                        <<
                ((dti.dt==DATA_TYPE::INT)? dti.val.dataAsInt : dti.val.dataAsLL);

            else if (dt.dt==INT64)
                dt.val.dataAsLL = dt.val.dataAsLL
                        <<
                ((dti.dt==DATA_TYPE::INT)? dti.val.dataAsInt : dti.val.dataAsLL);

            else assert(1==0);
        }
        else if (t.type() == TT::TRSh) {
            if (dt.dt==INT)
                dt.val.dataAsInt = dt.val.dataAsInt
                        >>
                ((dti.dt==DATA_TYPE::INT)? dti.val.dataAsInt : dti.val.dataAsLL);

            else if (dt.dt==INT64)
                dt.val.dataAsLL = dt.val.dataAsLL
                        >>
                ((dti.dt==DATA_TYPE::INT)? dti.val.dataAsInt : dti.val.dataAsLL);

            else assert(1==0);
        }
        else assert(1==0);

        t = scanner->peekToken();
    }

    return dt;
}

TData Parser::A()
{
    TData dt = B();
    assert(dt.dt==INT || dt.dt==INT64);

    Token t = scanner->peekToken();

    while (t.type() == TT::TPlus || t.type() == TT::TMinus) {
        scanner->nextToken();

        TData dti = B();
        assert(dti.dt==INT || dti.dt==INT64);

        if (dt.dt==INT && dti.dt==INT) {
            if (t.type() == TT::TPlus)
                dt.val.dataAsInt = dt.val.dataAsInt + dti.val.dataAsInt;
            else if (t.type() == TT::TMinus)
                dt.val.dataAsInt = dt.val.dataAsInt - dti.val.dataAsInt;
            else assert(0==1);
        }
        else if (dt.dt==INT64 || dti.dt==INT64) {
            if (t.type() == TT::TPlus)
                dt.val.dataAsLL =
                        ((dt.dt==DATA_TYPE::INT)?  dt.val.dataAsInt :  dt.val.dataAsLL)
                        +
                        ((dti.dt==DATA_TYPE::INT)? dti.val.dataAsInt : dti.val.dataAsLL);

            else if (t.type() == TT::TMinus)
                dt.val.dataAsLL =
                        ((dt.dt==DATA_TYPE::INT)?  dt.val.dataAsInt :  dt.val.dataAsLL)
                        -
                        ((dti.dt==DATA_TYPE::INT)? dti.val.dataAsInt : dti.val.dataAsLL);

            else assert(0==1);

            dt.dt = DATA_TYPE::INT64;
        }
        else assert(0==1);

        t = scanner->peekToken();
    }

    return dt;
}

// * /
TData Parser::B()
{
    TData dt = E();
    if (dt.dt != INT && dt.dt != INT64) {
        cerr << dt.dt << ' ' << dt.val.dataAsInt << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
        scanner->print_error("lol", "kek");
    }
    assert(dt.dt==INT || dt.dt==INT64);

    Token t = scanner->peekToken();

    while (t.type() == TT::TMult || t.type() == TT::TDiv) {
        scanner->nextToken();

        TData dti = E();
        assert(dti.dt==INT || dti.dt==INT64);

        /*          looks like compiler bug for me.
        if (t.type() == TT::TMult)
            ((dt.dt==DATA_TYPE::INT)? dt.val.dataAsInt : dt.val.dataAsLL) =
                    ((dt.dt==DATA_TYPE::INT)?  dt.val.dataAsInt :  dt.val.dataAsLL)
                    *
                    ((dti.dt==DATA_TYPE::INT)? dti.val.dataAsInt : dti.val.dataAsLL);

        else if (t.type() == TT::TDiv)
            ((dt.dt==DATA_TYPE::INT)? dt.val.dataAsInt : dt.val.dataAsLL) =
                    ((dt.dt==DATA_TYPE::INT)?  dt.val.dataAsInt :  dt.val.dataAsLL)
                    /
                    ((dti.dt==DATA_TYPE::INT)? dti.val.dataAsInt : dti.val.dataAsLL);

        else assert(0==1);

        if (dti.dt == INT64)
            dt.dt = INT64;
        //*/

        //*
        if (dt.dt==INT && dti.dt==INT) {
            if (t.type() == TT::TMult)
                dt.val.dataAsInt = dt.val.dataAsInt * dti.val.dataAsInt;
            else if (t.type() == TT::TDiv)
                dt.val.dataAsInt = dt.val.dataAsInt / dti.val.dataAsInt;
            else assert(0==1);
        }
        else if (dt.dt==INT64 || dti.dt==INT64) {
            if (t.type() == TT::TMult)
                dt.val.dataAsLL =
                        ((dt.dt==DATA_TYPE::INT)?  dt.val.dataAsInt :  dt.val.dataAsLL)
                        *
                        ((dti.dt==DATA_TYPE::INT)? dti.val.dataAsInt : dti.val.dataAsLL);

            else if (t.type() == TT::TDiv)
                dt.val.dataAsLL =
                        ((dt.dt==DATA_TYPE::INT)?  dt.val.dataAsInt :  dt.val.dataAsLL)
                        /
                        ((dti.dt==DATA_TYPE::INT)? dti.val.dataAsInt : dti.val.dataAsLL);

            else assert(0==1);

            dt.dt = DATA_TYPE::INT64;
        }
        else assert(0==1);
        //*/

        t = scanner->peekToken();
    }

    return dt;
}

// bug here consider case when flInt=0 and input is "a+1"
TData Parser::E()
{
    Token t = scanner->nextToken();

    TData ret;

    ret.dt=INT;         // del this to see the bug
    ret.val.dataAsInt = 0;// del this to see the bug

    if (t.type() == TT::TDecLiter || t.type() == TT::THexLiter) {

        long long val = std::stoll(t.lexeme(), nullptr, 0);     // here we decide INT or INT64
        if (val > INT_MAX) {
            ret.dt = DATA_TYPE::INT64;
            ret.val.dataAsLL = val;
        }
        else {
            ret.dt = DATA_TYPE::INT;
            ret.val.dataAsInt = val;
        }
    }
    else if (t.type() == TT::TMinus) {
        t = scanner->nextToken();
        if (t.type() != TT::TDecLiter && t.type() != TT::THexLiter) {
            scanner->print_error("number E", t);
        }

        long long val = -std::stoll(t.lexeme(), nullptr, 0);
        if (val < INT_MIN) {
            ret.dt = DATA_TYPE::INT64;
            ret.val.dataAsLL = val;
        }
        else {
            ret.dt = DATA_TYPE::INT;
            ret.val.dataAsInt = val;
        }
    }
    else if (t.type() == TT::TLParen) {             // (V)
        ret = V();
        t = scanner->nextToken();
        if (t.type() != TT::TRParen) {
            scanner->print_error(") E", t);
        }
    }
    else if (t.type() == TT::TIdent) {

        Node *node = nullptr;
        if (flInt) {                                // find in tree and check for var or arr
            node = st.findUp(t.lexeme());         // sem

            if (node == nullptr)
                scanner->print_error("Undefined variable", t.lexeme());

            ret.dt = node->dataType;
            if (node->dataType == DATA_TYPE::FUNC)
                scanner->print_error("function instead variable", t.lexeme());
        }

        t = scanner->peekToken();
        if (t.type() == TT::TLSqB) {
            if (flInt) {
                if (node->arrDim.size() == 0) {         // sem
                    scanner->print_error("Variable used as array", t.lexeme());
                }
            }

            scanner->nextToken();       // read [
            TData Tidx = V();                // check type and cast to int sem SEM

            t = scanner->nextToken();
            if (t.type() != TT::TRSqB) {
                scanner->print_error("] E", t);
            }

            if (flInt) {
                assert (Tidx.dt == DATA_TYPE::INT || Tidx.dt == DATA_TYPE::INT64);
                int idx = (int)((Tidx.dt == DATA_TYPE::INT)? Tidx.val.dataAsInt : Tidx.val.dataAsLL);

                if (idx < 0 || idx >= node->arrDim[0]) {
                    scanner->print_error("index out of range E", t);
                }

                switch (ret.dt) {
                    case INT:
                        ret.val.dataAsInt = node->dataVal.arrayInt[idx];
                    break;

                    case INT64:
                        ret.val.dataAsLL  = node->dataVal.arrayLL[idx];
                    break;

                    default: assert(0==3);break;
                }
            }
        }
        else {
            if (flInt) {
                if (node->arrDim.size() == 0) {
                    switch (ret.dt) {
                        case INT:
                            ret.val.dataAsInt = node->dataVal.dataAsInt;
                        break;

                        case INT64:
                            ret.val.dataAsLL  = node->dataVal.dataAsLL;
                        break;

                        default: assert(0==3);break;
                    }
                }
                else scanner->print_error("Array used as plain variable.", t.lexeme());
            }
        }
    }
    else {
        scanner->print_error("number,-,(,ident expected in E", t);
    }

    return ret;
}

//  a = V;
void assignment(Node *node, TData data)
{
    if (node->dataType==INT)
        node->dataVal.dataAsInt = ((data.dt==INT)? data.val.dataAsInt : data.val.dataAsLL);

    else if (node->dataType==INT64)
        node->dataVal.dataAsLL = ((data.dt==INT)? data.val.dataAsInt : data.val.dataAsLL);

    else assert(1==0);
}

// for (a = V; V; a = V) O
void Parser::U()
{
    Token t = scanner->nextToken();
    if (t.type() != TT::TFor)
        scanner->print_error("For U", t);

    t = scanner->nextToken();
    if (t.type() != TT::TLParen)
        scanner->print_error("( U", t);

    t = scanner->nextToken();
    if (t.type() != TT::TIdent)
        scanner->print_error("Identifier U", t);

    Node *node = nullptr;

    if (flInt) {
        node = st.findUp(t.lexeme());

        if (node == nullptr)
            scanner->print_error("undefined variable", t);
    }

    t = scanner->nextToken();
    if (t.type() != TT::TAssign)
        scanner->print_error("= U", t);

    TData dt = V();

    if (flInt)
        assignment(node, dt);

    t = scanner->nextToken();
    if (t.type() != TT::TSc)
        scanner->print_error("; U", t);

    int scannerCond = scanner->getPos();            // condition check in for-loop
    TData checkRes = V();

    t = scanner->nextToken();
    if (t.type() != TT::TSc)
        scanner->print_error("; U", t);

    int scannerMod = scanner->getPos();
    t = scanner->nextToken();
    if (t.type() != TT::TIdent)
        scanner->print_error("Identifier U", t);

    node = nullptr;
    if (flInt) {
        node = st.findUp(t.lexeme());                // third part of for-loop header
        if (node == nullptr)
            scanner->print_error("undefined variable", t);
    }

    t = scanner->nextToken();
    if (t.type() != TT::TAssign)
        scanner->print_error("= U", t);

    bool flIntGlobal = flInt;
    flInt = false;
    V();

    t = scanner->nextToken();
    if (t.type() != TT::TRParen)
        scanner->print_error(") U", t);

    int scannerLoopBody = scanner->getPos();
    O();
    int scannerAfterLoop = scanner->getPos();

    flInt = flIntGlobal;
    int condition = ((checkRes.dt==INT)? checkRes.val.dataAsInt : checkRes.val.dataAsLL);
    if (flInt) {
        while (condition != 0) {
            scanner->setPos(scannerLoopBody);
            O();

            scanner->setPos(scannerMod);
            t = scanner->nextToken();               // identifier!
            node = st.findUp(t.lexeme());         // third part of for-loop header
            if (node == nullptr)
                scanner->print_error("undefined variable", t);
            t = scanner->nextToken();               // '=' sign
            //t = scanner->nextToken();               //
            TData dt = V();
            assignment(node, dt);                   // a = V;

            scanner->setPos(scannerCond);
            checkRes = V();
            condition = ((checkRes.dt==INT)? checkRes.val.dataAsInt : checkRes.val.dataAsLL);
            assert(flInt == true);
        }
    }

    if (flInt)
        scanner->setPos(scannerAfterLoop);
}





