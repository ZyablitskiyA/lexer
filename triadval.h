#ifndef TRIADVAL_H
#define TRIADVAL_H
#include "semtree.h"
#include <string>

enum TriadValType {
    Empty, LinkToTree, LinkToTriad, Literal
};

class TriadVal
{
public:
    TriadVal();
    TriadVal(const TData &_literal);
    TriadVal(int _triad_idx);
    TriadVal(Node *_tree_node);

    bool isEmpty() const;
    string toString() const;

    TriadValType getType();

    void set(const TData &_literal);
    void set(int _triad_idx);
    void set(Node *_tree_node);

//private:
    TriadValType val_type;
    union {
        Node *tree_node;
        int triad_idx;
        TData literal;
    };

    //data_type for triad idx
    string toAsmString();

    bool is_empty;
};

#endif // TRIADVAL_H
