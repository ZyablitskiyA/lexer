#ifndef SEMTREE_H
#define SEMTREE_H

#include <string>
#include <vector>
#include "scanner.h"

using std::string;

enum DATA_TYPE {
    INT,
    INT64,
    FUNC
};

union DataVal {
    int dataAsInt;
    long long dataAsLL;
    /*
    int *arrayInt;              // delit
    long long *arrayLL;              // delit
    int scannerPos;              // delit ??
    */
};

struct TData {
    TData() { }

    TData(DATA_TYPE dt, DataVal dv)
    {
        this->dt = dt;
        this->val = dv;
    }

    DATA_TYPE dt;
    DataVal val;
};

class Node
{
public:
    Node();

    //SemTree *tree;

    std::string lex;
    DATA_TYPE dataType;
    //DataVal dataVal;
    std::vector<int> arrDim;

    // Defined only for functions. Idx of the first triad of that function.
    int func_triad_idx;

    Node *left;
    Node *right;
    Node *parent;

    Node *createLeft();
    Node *createRight();

    //---- for asm lab
    bool isGlobal;
    string address;         // [ebp-4] for locals or a245 for globals
    //---^ for asm lab

    ~Node();
};

class SemTree
{
private:

    Node *root;
    Node *cur;

    void print_error(const string &m1, const string &m2) const;

    void _printTree(Node *n, int indent);

public:
    SemTree();

    lexer::Scanner *scanner2printErrors;

    void (*printError)(const string &m1, const string &m2);
    Node* getRoot();

    Node* getCur();
    void setCur(Node *newCur);

    Node* findSameLevel(Node *from, const std::string &lex);
    bool isDuplicate(Node *from, const std::string &lex);

    Node* semInclude(const string &lex, DATA_TYPE dt);
    void CompoundOpCreateScope();
    void LeaveOneScope();
    void semAddArrDimansion(int dim);

    Node *findUp(const string &str);

    void printTree();
    void deleteSubTree(Node *n);
};

#endif // SEMTREE_H
