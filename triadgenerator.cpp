#include "triadgenerator.h"
#include <fstream>
#include <cassert>
#include "codegenerator.h"

using namespace std;

Triad::Triad(const string &_operation, const TriadVal &_op1, const TriadVal &_op2)
    : operation(_operation), op1(_op1), op2(_op2)
{

}

TriadGenerator::TriadGenerator()
{

}

void TriadGenerator::add_operand(const TriadVal &operand)
{
    operands.push(operand);
}

void TriadGenerator::add_operation(const string &op)
{
    operations.push(op);
}

// Only for literal. Assumes literal on top of operands stack!
void TriadGenerator::apply_unary_minus()
{
    TriadVal &val = operands.top();
    assert(val.val_type == Literal);

    if (val.literal.dt == INT)
        val.literal.val.dataAsInt = -val.literal.val.dataAsInt;
    else if (val.literal.dt == INT64)
        val.literal.val.dataAsLL = -val.literal.val.dataAsLL;
    else throw 1;
}

void TriadGenerator::generate_arithm_triad()
{
    string operation = operations.top(); operations.pop();
    TriadVal op2 = operands.top(); operands.pop();
    TriadVal op1 = operands.top(); operands.pop();
    triads.push_back(Triad(operation, op1, op2));
    operands.push(TriadVal((int)triads.size()-1));
}

void TriadGenerator::generate_assignment()
{
    TriadVal op2 = operands.top(); operands.pop();
    TriadVal op1 = operands.top(); operands.pop();
    triads.push_back(Triad("=", op1, op2));
}

void TriadGenerator::gen_empty_if_and_save_addr()
{
    for_if_address.push((int)triads.size());
    triads.push_back(Triad("IF", TriadVal(), TriadVal()));
}

void TriadGenerator::add_proc(Node* func)
{
    assert(func->dataType == DATA_TYPE::FUNC);
    triads.push_back(Triad("PROC", TriadVal(func), TriadVal()));
}

void TriadGenerator::add_ret_endp()
{
    triads.push_back(Triad("RET", TriadVal(), TriadVal()));
    triads.push_back(Triad("ENDP", TriadVal(), TriadVal()));
}

int TriadGenerator::get_last_triad_idx()
{
    return (int)triads.size()-1;
}

void TriadGenerator::printTriads(const string &file_name)
{
    printTriads(triads, file_name);
}

void TriadGenerator::printTriads(const vector<Triad> &triads, const string &file_name)
{
    ofstream opf(file_name);
    for (int i = 0; i < (int)triads.size(); i++) {
        const Triad &cur = triads[i];

        opf << to_string(i+1) + ") " + cur.operation + " " +
               cur.op1.toString() + " " + cur.op2.toString() << endl;

    }
}

void TriadGenerator::remember_cond_address()
{
    for_cond_address.push((int)triads.size());
}

void TriadGenerator::remember_inc_address()
{
    for_inc_address.push((int)triads.size());
}

void TriadGenerator::add_array(Node * arrayInTree)
{
    assert(arrayInTree->arrDim.size() > 0);

    arrays.push(arrayInTree);
}

void TriadGenerator::gen_arr_deref()
{
    triads.push_back(Triad("DEREF", TriadVal(arrays.top()), TriadVal(operands.top())));
    arrays.pop(); operands.pop();
    operands.push(TriadVal((int)triads.size()-1));
}


void TriadGenerator::pop_operand()
{
    operands.pop();
}

void TriadGenerator::gen_goto_cond()
{
    triads.push_back(Triad("GOTO", TriadVal(for_cond_address.top()), TriadVal()));
    for_cond_address.pop();
}

void TriadGenerator::gen_goto_inc()
{
    triads.push_back(Triad("GOTO", TriadVal(for_inc_address.top()), TriadVal()));
    for_inc_address.pop();
}

void TriadGenerator::generate_nop()
{
    triads.push_back(Triad("NOP", TriadVal(), TriadVal()));
}

void TriadGenerator::set_next_addr_to_last_if()
{
    int addr = triads.size();
    Triad &tr = triads[for_if_address.top()];
    if (tr.op1.isEmpty()) {
        tr.op1.set(addr);
    }
    else if (tr.op2.isEmpty()) {
        tr.op2.set(addr);
        for_if_address.pop();
    }
    else throw 1;
}

void TriadGenerator::gen_function_call(Node * node)
{
    assert(node->dataType == DATA_TYPE::FUNC);

    triads.push_back(Triad("CALL", TriadVal(node), TriadVal()));
}

vector<Triad> TriadGenerator::getTriads()
{
    return triads;
}
