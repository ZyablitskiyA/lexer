TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    scanner.cpp \
    token.cpp \
    lexer_defs.cpp \
    semtree.cpp \
    llparser.cpp \
    triadgenerator.cpp \
    triadval.cpp \
    optimizer.cpp \
    codegenerator.cpp

HEADERS += \
    lexer_defs.h \
    scanner.h \
    token.h \
    semtree.h \
    llparser.h \
    triadgenerator.h \
    triadval.h \
    optimizer.h \
    codegenerator.h

DISTFILES += \
    ../build-lexer-Desktop-Debug/input.txt \
    input.txt \
    parserTest.txt \
    triads.txt \
    sup_tests.txt \
    ../build-lexer-Desktop-Debug/optimized.txt

