#include <iostream>
#include <iomanip>
#include "lexer_defs.h"
#include "token.h"
#include "parser.h"
#include "scanner.h"
#include "llparser.h"
#include "optimizer.h"
#include "codegenerator.h"

using namespace std;
using namespace lexer;

lexer::Token get_token()
{
    return lexer::Token("void", lexer::TIdent);
}


string tt2text(TokenType tt)
{
    switch (tt) {
        case TFor: return "TFor";
        case TAssign: return "TAssign";
        case TSc: return "TSc";
        case TComma: return "TComma";
        case TInt: return "TInt";
        case TLL: return "TLL";
        case TVoid: return "TVoid";
        case TIdent: return "TIdent";
        case TDecLiter: return "TDecLiter";
        case THexLiter: return "THexLiter";
        case TLParen: return "TLParen";
        case TRParen: return "TRParen";
        case TLBrace: return "TLBrace";
        case TRBrace: return "TRBrace";
        case TLSqB: return "TLSqB";
        case TRSqB: return "TRSqB";

        case TLT: return "TLT";
        case TLE: return "TLE";
        case TGT: return "TGT";
        case TGE: return "TGE";
        case TEq: return "TEq";
        case TNEq: return "TNEq";

        case TLSh: return "TLSh";
        case TRSh: return "TRSh";
        case TPlus: return "TPlus";
        case TMinus: return "TMinus";
        case TMult: return "TMult";
        case TDiv: return "TDiv";

        case TEnd: return "TEnd";
        case TErr: return "TErr";
        default: cerr << "error in tt2text" << endl;
        return "error in tt2text";
    }
}

int main()
{

    lexer::Scanner sc("../lexer/parserTest.txt");

    //Token t;

    /*  Lexer testing code
    while ((t = sc.nextToken()).type() != TokenType::TEnd) {
        cout << setw(10) << t.lexeme() << ' ' << tt2text(t.type()) << endl;
        //t = sc.peekToken();
        //cout << setw(10) << t.lexeme() << ' ' << tt2text(t.type()) << endl;
    }
    //*/

    //Parser p(&sc);
    //p.P();
    //p.st.printTree();

    LlParser parser(&sc);
    parser.triad_gen->printTriads("../lexer/triads.txt");
    parser.sem_tree->printTree();
    Optimizer opt;
    vector<Triad> copy = parser.triad_gen->getTriads();
    CodeGenerator gen;
    gen.generateAsm(copy, parser.sem_tree);
    //TriadGenerator::printTriads(opt.optimize_loops(copy), "optimized.txt");
    return 0;
}

