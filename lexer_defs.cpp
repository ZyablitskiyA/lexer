#include "lexer_defs.h"

#include <string>
#include <vector>
#include <iostream>

using namespace std;
using namespace lexer;

TokenType lexer::get_key_word_type(const string &lexeme)
{
    static const vector<pair<string, TokenType> > key_word = {
        make_pair("int", TokenType::TInt),
        make_pair("__int64", TokenType::TLL),
        make_pair("void", TokenType::TVoid),
        make_pair("for", TokenType::TFor)
    };

    for (int i = 0; i < key_word.size(); i++) {
        if (lexeme == key_word[i].first) return key_word[i].second;
    }

    return TokenType::TIdent;
}
