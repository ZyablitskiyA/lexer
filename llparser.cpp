#include <cassert>
#include <iostream>
#include <climits>
#include "llparser.h"

#define TT lexer::TokenType

using namespace lexer;
using namespace std;

Symbol::Symbol(lexer::TokenType _t) : type(Term), t(_t)
{ }

Symbol::Symbol(NeTerm _nt) : type(Neterm), nt(_nt)
{ }

Symbol::Symbol(SemProc _sem) : type(Sem), sem(_sem)
{ }

SymbolType Symbol::getType()
{
    return type;
}

LlParser::LlParser(lexer::Scanner *_sc) : sc(_sc)
{
    cerr << "You have foregotten to implement <= >=. tsss" << endl;
    triad_gen = new TriadGenerator();
    sem_tree = new SemTree();
    sem_tree->scanner2printErrors = _sc;
    doYourJob();
}

void LlParser::doYourJob()
{
    assert(st.empty());
    st.push(Symbol(NeTerm::P));
    DATA_TYPE last_data_type;

    Token cur = sc->nextToken();
    Token prev_token;
    Node *last_decl_var = nullptr;                            // last defined variable or array
    Node *prev_token_tree = nullptr;                            // needed in E1, K for array/not array check
    bool jobDone = false;
//int qweqwe = 1;
    while (!jobDone) {
//std::cerr << qweqwe++ << endl;
        Symbol s = st.top();
        if (s.getType() == SymbolType::Neterm) {
            switch(s.nt) {
            case P:
                if (cur.type() == TT::TInt || cur.type() == TT::TLL) {
                    st.push(NeTerm::D);
                }
                else if (cur.type() == TT::TVoid) {
                    st.push(NeTerm::G);
                }
                else if (cur.type() == TT::TEnd) {
                    jobDone = true;
                }
                else
                    sc->print_error("P", "unexpected shit");
                break;

            case D:
                st.pop();
                st.push(Symbol(TT::TSc));
                st.push(Symbol(NeTerm::Z));
                st.push(Symbol(SemProc::IncludeVar));
                st.push(Symbol(TT::TIdent));

                if (cur.type() == TT::TInt)
                    st.push(Symbol(TT::TInt));
                else if (cur.type() == TT::TLL)
                    st.push(Symbol(TT::TLL));
                else
                    sc->print_error("D", "unexpected shit");
                break;

            case Z:
                if (cur.type() == TT::TComma) {
                    st.push(Symbol(SemProc::IncludeVar));
                    st.push(Symbol(TT::TIdent));
                    st.push(Symbol(TT::TComma));
                }
                else if (cur.type() == TT::TAssign) {
                    st.pop();
                    st.push(Symbol(NeTerm::C));
                    st.push(Symbol(SemProc::GenAssignment));
                    st.push(Symbol(NeTerm::V));
                    st.push(Symbol(SemProc::LastVarToOperands));
                    st.push(Symbol(TT::TAssign));
                }
                else if (cur.type() == TT::TLSqB) {
                    st.pop();
                    st.push(Symbol(NeTerm::C));
                    st.push(Symbol(TT::TRSqB));
                    st.push(Symbol(SemProc::AddArrDim));
                    st.push(Symbol(TT::TDecLiter));             // or Hex
                    st.push(Symbol(TT::TLSqB));
                }
                else if (cur.type() == TT::TSc) {               // follow (Z) = {;}
                    st.pop();
                }
                else
                    sc->print_error("Z", "unexpected shit");
                break;

            case C:
                if (cur.type() == TT::TComma) {
                    st.pop();
                    st.push(Symbol(NeTerm::Z));
                    st.push(Symbol(SemProc::IncludeVar));
                    st.push(Symbol(TT::TIdent));
                    st.push(Symbol(TT::TComma));
                }
                else if (cur.type() == TT::TSc) {
                    st.pop();
                }
                else
                    sc->print_error("C", "123");
                break;

            case G:
                if (cur.type() == TT::TVoid) {
                    st.pop();
                    st.push(Symbol(SemProc::LeaveOneScope));
                    st.push(Symbol(TT::TRBrace));
                    st.push(Symbol(SemProc::GenerateEndp));
                    st.push(Symbol(NeTerm::Q));
                    st.push(Symbol(SemProc::SaveFunctionIdx));
                    st.push(Symbol(SemProc::GenerateProc));
                    st.push(Symbol(TT::TLBrace));
                    st.push(Symbol(TT::TRParen));
                    st.push(Symbol(TT::TLParen));
                    st.push(Symbol(SemProc::IncludeVar));
                    st.push(Symbol(TT::TIdent));
                    st.push(Symbol(TT::TVoid));
                }
                else
                    sc->print_error("G", "123");
                break;

            case V:
                st.pop();
                st.push(Symbol(NeTerm::V1));
                st.push(Symbol(NeTerm::M));
                break;

            case V1:                                        // bug prone. why?
                st.pop();
                if (cur.type() == TT::TEq) {
                    st.push(Symbol(NeTerm::V1));
                    st.push(Symbol(SemProc::GenEq));
                    st.push(Symbol(NeTerm::M));
                    st.push(Symbol(TT::TEq));
                }
                else if (cur.type() == TT::TNEq) {
                    st.push(Symbol(NeTerm::V1));
                    st.push(Symbol(SemProc::GenNEq));
                    st.push(Symbol(NeTerm::M));
                    st.push(Symbol(TT::TNEq));
                }
                else if (cur.type() == TT::TIdent || cur.type() == TT::TSc ||           // bps2
                         cur.type() == TT::TRSqB || cur.type() == TT::TRParen || cur.type() == TT::TComma) {
                }
                else
                    sc->print_error("V1", "123");
                break;

            case M:
                st.pop();
                st.push(Symbol(NeTerm::M1));
                st.push(Symbol(NeTerm::S));
                break;

            case M1:
                st.pop();
                if (cur.type() == TT::TGT) {                // TGT == > ???????
                    st.push(Symbol(NeTerm::M1));
                    st.push(Symbol(SemProc::GenGT));
                    st.push(Symbol(NeTerm::S));
                    st.push(Symbol(TT::TGT));
                }
                else if (cur.type() == TT::TLT) {           // TLT == <
                    st.push(Symbol(NeTerm::M1));
                    st.push(Symbol(SemProc::GenLT));
                    st.push(Symbol(NeTerm::S));
                    st.push(Symbol(TT::TLT));
                }
                else if (cur.type() == TT::TIdent || cur.type() == TT::TSc ||
                         cur.type() == TT::TRSqB || cur.type() == TT::TRParen ||
                         cur.type() == TT::TEq || cur.type() == TT::TNEq || cur.type() == TT::TComma) {
                }
                else
                    sc->print_error("M", "123");
                break;

            case S:
                st.pop();
                st.push(Symbol(NeTerm::S1));
                st.push(Symbol(NeTerm::A));
                break;

            case S1:
                st.pop();
                if (cur.type() == TT::TLSh) {
                    st.push(Symbol(NeTerm::S1));
                    st.push(Symbol(SemProc::GenLSh));
                    st.push(Symbol(NeTerm::A));
                    st.push(Symbol(TT::TLSh));
                }
                else if (cur.type() == TT::TRSh) {
                    st.push(Symbol(NeTerm::S1));
                    st.push(Symbol(SemProc::GenRSh));
                    st.push(Symbol(NeTerm::A));
                    st.push(Symbol(TT::TRSh));
                }
                else if (cur.type() == TT::TIdent || cur.type() == TT::TSc ||
                         cur.type() == TT::TRSqB || cur.type() == TT::TRParen ||
                         cur.type() == TT::TEq || cur.type() == TT::TNEq ||
                         cur.type() == TT::TGT || cur.type() == TT::TLT || cur.type() == TT::TComma) {
                }
                else
                    sc->print_error("S", "123");
                break;

            case A:
                st.pop();
                st.push(Symbol(NeTerm::A1));
                st.push(Symbol(NeTerm::B));
                break;

            case A1:
                st.pop();
                if (cur.type() == TT::TPlus) {
                    st.push(Symbol(NeTerm::A1));
                    st.push(Symbol(SemProc::GenPlus));
                    st.push(Symbol(NeTerm::B));
                    st.push(Symbol(TT::TPlus));
                }
                else if (cur.type() == TT::TMinus) {
                    st.push(Symbol(NeTerm::A1));
                    st.push(Symbol(SemProc::GenMinus));
                    st.push(Symbol(NeTerm::B));
                    st.push(Symbol(TT::TMinus));
                }
                else if (cur.type() == TT::TIdent || cur.type() == TT::TSc ||
                         cur.type() == TT::TRSqB || cur.type() == TT::TRParen ||
                         cur.type() == TT::TEq || cur.type() == TT::TNEq ||
                         cur.type() == TT::TGT || cur.type() == TT::TLT ||
                         cur.type() == TT::TLSh || cur.type() == TT::TRSh || cur.type() == TT::TComma) {

                }
                else
                    sc->print_error("A", "123");
                break;

            case B:
                st.pop();
                st.push(Symbol(NeTerm::B1));
                st.push(Symbol(NeTerm::E));
                break;

            case B1:
                st.pop();
                if (cur.type() == TT::TMult) {
                    st.push(Symbol(NeTerm::B1));
                    st.push(Symbol(SemProc::GenMult));
                    st.push(Symbol(NeTerm::E));
                    st.push(Symbol(TT::TMult));
                }
                else if (cur.type() == TT::TDiv) {
                    st.push(Symbol(NeTerm::B1));
                    st.push(Symbol(SemProc::GenDiv));
                    st.push(Symbol(NeTerm::E));
                    st.push(Symbol(TT::TDiv));
                }
                else if (cur.type() == TT::TIdent || cur.type() == TT::TSc ||
                         cur.type() == TT::TRSqB || cur.type() == TT::TRParen ||
                         cur.type() == TT::TEq || cur.type() == TT::TNEq ||
                         cur.type() == TT::TGT || cur.type() == TT::TLT ||
                         cur.type() == TT::TLSh || cur.type() == TT::TRSh ||
                         cur.type() == TT::TPlus || cur.type() == TT::TMinus || cur.type() == TT::TComma) {
                }
                else
                    sc->print_error("B", "123");
                break;

            case E:     // ok?
                if (cur.type() == TT::TDecLiter || cur.type() == TT::THexLiter) {
                    st.pop();
                    st.push(Symbol(cur.type()));
                    st.push(Symbol(SemProc::ProcessLiteral));
                }
                else if (cur.type() == TT::TMinus) {
                    st.pop();
                    st.push(Symbol(TT::TDecLiter));                     // oh shit =(
                    st.push(Symbol(SemProc::UnaryMinus));
                    st.push(Symbol(SemProc::ProcessLiteral));
                    st.push(Symbol(TT::TMinus));
                }
                else if (cur.type() == TT::TLParen) {
                    st.pop();
                    st.push(Symbol(TT::TRParen));
                    st.push(Symbol(NeTerm::V));
                    st.push(Symbol(TT::TLParen));
                }
                else if (cur.type() == TT::TIdent) {
                    st.pop();
                    st.push(Symbol(NeTerm::E1));
                    st.push(Symbol(SemProc::FindInTree));
                    st.push(Symbol(TT::TIdent));
                }
                else
                    sc->print_error("E", "123");
                break;

            case E1:
                if (cur.type() == TT::TLSqB) {
                    if (prev_token_tree->arrDim.size() == 0) {
                        sc->print_error("plain var or function used as array", prev_token.lexeme());
                    }

                    // we assume here that array name was read preveously and prev_token_tree was set by SemProc::FindInTree
                    // and this array tree node was pushed to array stack in triad_gen by SemProc::FindInTree.

                    st.pop();
                    st.push(Symbol(SemProc::GenArrDerefTriad));
                    st.push(Symbol(TT::TRSqB));
                    st.push(Symbol(NeTerm::V));
                    st.push(Symbol(TT::TLSqB));
                }
                else if (cur.type() == TT::TIdent || cur.type() == TT::TSc ||
                         cur.type() == TT::TRSqB || cur.type() == TT::TRParen ||
                         cur.type() == TT::TEq || cur.type() == TT::TNEq ||
                         cur.type() == TT::TGT || cur.type() == TT::TLT ||
                         cur.type() == TT::TLSh || cur.type() == TT::TRSh ||
                         cur.type() == TT::TPlus || cur.type() == TT::TMinus ||
                         cur.type() == TT::TMult || cur.type() == TT::TDiv || cur.type() == TT::TComma) {

                    st.pop();
                    if (prev_token_tree->dataType == DATA_TYPE::FUNC)
                        sc->print_error("Function used as plain var", prev_token_tree->lex);
                    if (prev_token_tree->arrDim.size() > 0)
                        sc->print_error("Array used as plain var", prev_token_tree->lex);
                    triad_gen->add_operand(TriadVal(prev_token_tree));      // variable (not array or func) pushed to operand stack
                }
                else
                    sc->print_error("E1", "123");
                break;

            case O:
                if (cur.type() == TT::TIdent) {
                    st.pop();
                    st.push(Symbol(NeTerm::K));
                    st.push(Symbol(SemProc::FindInTree));
                    st.push(Symbol(TT::TIdent));
                }
                else if (cur.type() == TT::TSc) {
                    st.pop();
                    st.push(Symbol(TT::TSc));
                }
                else if (cur.type() == TT::TFor) {
                    st.pop();
                    st.push(Symbol(NeTerm::U));
                }
                else if (cur.type() == TT::TLBrace) {
                    st.pop();
                    st.push(Symbol(TT::TRBrace));
                    st.push(Symbol(SemProc::LeaveOneScope));        // go top one level
                    st.push(Symbol(NeTerm::Q));
                    st.push(Symbol(SemProc::NestScope));        // we need to go deeper.
                    st.push(Symbol(TT::TLBrace));
                }
                else
                    sc->print_error("O", "123");
                break;

            case K:
                // we assume here that identifier was read preveously and prev_token_tree was set by SemProc::FindInTree
                // and if this ident corresponds to an array, its tree node was pushed to array stack in triad_gen by SemProc::FindInTree.

                if (cur.type() == TT::TLSqB) {
                    if (prev_token_tree->arrDim.size() == 0)
                        sc->print_error("Array expected. Got ", prev_token_tree->lex);

                    st.pop();
                    st.push(Symbol(TT::TSc));
                    st.push(Symbol(SemProc::GenAssignment));
                    st.push(Symbol(NeTerm::V));
                    st.push(Symbol(TT::TAssign));
                    st.push(Symbol(SemProc::GenArrDerefTriad));
                    st.push(Symbol(TT::TRSqB));
                    st.push(Symbol(NeTerm::V));
                    st.push(Symbol(TT::TLSqB));
                }
                else if (cur.type() == TT::TAssign) {
                    if (prev_token_tree->arrDim.size() > 0 || prev_token_tree->dataType == DATA_TYPE::FUNC)
                        sc->print_error("Plain variable expected. Got ", prev_token_tree->lex);
                    triad_gen->add_operand(prev_token_tree);

                    st.pop();
                    st.push(Symbol(TT::TSc));
                    st.push(Symbol(SemProc::GenAssignment));
                    st.push(Symbol(NeTerm::V));
                    st.push(Symbol(TT::TAssign));
                }
                else if (cur.type() == TT::TLParen) {
                    if (prev_token_tree->dataType != DATA_TYPE::FUNC)
                        sc->print_error("Function expected. Got ", prev_token_tree->lex);
                    triad_gen->gen_function_call(prev_token_tree);
                    triad_gen->generate_nop();          // RET puts to IP succeeding address after function call

                    st.pop();
                    st.push(Symbol(TT::TSc));
                    st.push(Symbol(TT::TRParen));
                    st.push(Symbol(TT::TLParen));
                }
                else
                    sc->print_error("K", "123");
                break;

            case U:
                if (cur.type() == TT::TFor) {
                    st.pop();

                    st.push(Symbol(SemProc::GenNop));
                    st.push(Symbol(SemProc::SetNextAddrToLastIf));      // put next addr (of nop) to remembered if as second branch
                    st.push(Symbol(SemProc::GenGotoInc));
                    st.push(Symbol(NeTerm::O));
                    st.push(Symbol(SemProc::GenNop));
                    st.push(Symbol(SemProc::SetNextAddrToLastIf));      // put next addr (of nop) to remembered if

                    st.push(Symbol(TT::TRParen));
                    st.push(Symbol(SemProc::GenGotoCond));              // go and check loop condition after increment
                    st.push(Symbol(SemProc::GenAssignment));
                    st.push(Symbol(NeTerm::V));
                    st.push(Symbol(TT::TAssign));
                    st.push(Symbol(SemProc::PushOpPrev_token_tree));
                    st.push(Symbol(SemProc::FindInTree));
                    st.push(Symbol(TT::TIdent));
                    st.push(Symbol(SemProc::GenNop));                   // TODO: test this. BUG PRONE. Needed for optimizer. tested a bit.
                    st.push(Symbol(SemProc::RememberIncAddress));

                    st.push(Symbol(SemProc::GenEmptyIf));
                    st.push(Symbol(SemProc::ForceArithmFlags));         // generate + 0 triad and pop result. Gurantees at least one triad
                    st.push(Symbol(TT::TSc));
                    st.push(Symbol(NeTerm::V));                         // TODO: may be pop the result? Done!!
                    st.push(Symbol(SemProc::GenNop));                   // TODO: test this. BUG PRONE. Needed for optimizer. tested a bit.
                    st.push(Symbol(SemProc::RememberCondAddress));      // remembers next triad address to special stack

                    st.push(Symbol(TT::TSc));
                    st.push(Symbol(SemProc::GenAssignment));
                    st.push(Symbol(NeTerm::V));
                    st.push(Symbol(TT::TAssign));
                    st.push(Symbol(SemProc::PushOpPrev_token_tree));
                    st.push(Symbol(SemProc::FindInTree));
                    st.push(Symbol(TT::TIdent));
                    st.push(Symbol(TT::TLParen));
                    st.push(Symbol(TT::TFor));
                }
                else
                    sc->print_error("U", "123");
                break;

            case Q:
                if (cur.type() == TT::TIdent || cur.type() == TT::TSc ||
                    cur.type() == TT::TFor || cur.type() == TT::TLBrace) {
                    st.push(Symbol(NeTerm::O));
                }
                else if (cur.type() == TT::TInt || cur.type() == TT::TLL) {
                    st.push(Symbol(NeTerm::D));
                }
                else if (cur.type() == TT::TRBrace) {     // follow Q = }
                    st.pop();
                }
                else
                    sc->print_error("Q", "123");
                break;
            }
        }
        else if (s.getType() == SymbolType::Term) {

            if (s.t == cur.type() || ((s.t == TT::THexLiter || s.t == TT::TDecLiter) &&
                                      (cur.type() == TT::THexLiter || cur.type() == TT::TDecLiter)))
            {
                if (cur.type() == TT::TInt)
                    last_data_type = DATA_TYPE::INT;
                else if (cur.type() == TT::TLL)
                    last_data_type = DATA_TYPE::INT64;
                else if (cur.type() == TT::TVoid)
                    last_data_type = DATA_TYPE::FUNC;

                prev_token = cur;
                cur = sc->nextToken();
                st.pop();
            }
            else
                sc->print_error("Terminal mismatch", "123");
        }
        else if (s.getType() == SymbolType::Sem) {
            switch (s.sem) {
                case ProcessLiteral:
                {
                    st.pop();
                    TData liter_val;
                    long long val = std::stoll(cur.lexeme(), nullptr, 0);
                    if (val > INT_MAX) {
                        liter_val.dt = DATA_TYPE::INT64;
                        liter_val.val.dataAsLL = val;
                    }
                    else {
                        liter_val.dt = DATA_TYPE::INT;
                        liter_val.val.dataAsInt = val;
                    }
                    triad_gen->add_operand(TriadVal(liter_val));
                }
                    break;

                case SemProc::RememberCondAddress:
                    st.pop();
                    triad_gen->remember_cond_address();     // remember triad index of for-loop condition.
                break;

                case SemProc::UnaryMinus:
                    st.pop();
                    triad_gen->apply_unary_minus(); // works only with literals!!!
                    break;

                // Add variable/array name to SemTree and save pointer to node
                case SemProc::IncludeVar:
                    st.pop();
                    last_decl_var = sem_tree->semInclude(prev_token.lexeme(), last_data_type);
                break;

                // TODO where is it used?
                case SemProc::LastVarToOperands:
                    st.pop();
                    triad_gen->add_operand(TriadVal(last_decl_var));
                break;

                case SemProc::AddArrDim:
                    st.pop();
                    sem_tree->semAddArrDimansion(std::stoi(prev_token.lexeme(), nullptr, 0));
                break;

                case SemProc::GenAssignment:
                    st.pop();
                    triad_gen->generate_assignment();
                break;

                // Used at the end of function or compound operator for going to parent scope in sem_tree
                case SemProc::LeaveOneScope:
                    st.pop();
                    sem_tree->LeaveOneScope();
                break;

                // assumes last_decl_var be a pointer to FUNC sem_tree node.
                // generates PROC <FUNC NAME> triad
                case SemProc::GenerateProc:
                    st.pop();
                    triad_gen->add_proc(last_decl_var);
                break;

                // assumes last_decl_var be a pointer to FUNC sem_tree node.
                // save to last_decl_var (it is sem_tree) idx of the first triad of the function body for func calls in the future.
                case SemProc::SaveFunctionIdx:
                    st.pop();
                    assert(last_decl_var->dataType == DATA_TYPE::FUNC);
                    last_decl_var->func_triad_idx = triad_gen->get_last_triad_idx() + 1; // triad must be generated asap
                break;

                // Generates closing triads (ret endp) of a function
                case SemProc::GenerateEndp:
                    st.pop();
                    triad_gen->add_ret_endp();
                break;

                // To check that a variable was previously declared. If it is an array it is pushed to stack in triad_gen.
                case SemProc::FindInTree:
                {
                    assert(prev_token.type() == TIdent);
                    st.pop();
                    Node *ptr = sem_tree->findUp(prev_token.lexeme());
                    if (ptr == nullptr)
                        sc->print_error("Undefined variable", prev_token.lexeme());
                    else {
                        if (ptr->arrDim.size()>0)
                            triad_gen->add_array(ptr);
                        prev_token_tree = ptr;
                    }
                }
                break;

                // Generates +0 triad.
                // It is used in For-loop cond block to set flag register which could be not set
                // in case when expression is NeTerm::E only
                case SemProc::ForceArithmFlags:
                {
                    DataVal zero;
                    zero.dataAsInt = 0;

                    st.pop();
                    triad_gen->add_operation("+");
                    triad_gen->add_operand(TriadVal(TData(DATA_TYPE::INT, zero)));
                    triad_gen->generate_arithm_triad();
                    triad_gen->pop_operand();                   // since it is used only to set flags, result of this operation will not be used.
                    break;
                }

                // shit
                case SemProc::PushOpPrev_token_tree:
                    st.pop();
                    triad_gen->add_operand(prev_token_tree);
                    break;

                case SemProc::GenEmptyIf:
                    st.pop();
                    triad_gen->gen_empty_if_and_save_addr();
                    break;

                case SemProc::GenGotoCond:
                    st.pop();
                    triad_gen->gen_goto_cond();
                    break;

                case SemProc::GenArrDerefTriad:
                    st.pop();
                    triad_gen->gen_arr_deref();
                    break;

                case SemProc::RememberIncAddress:
                    st.pop();
                    triad_gen->remember_inc_address();
                    break;

                case SemProc::GenGotoInc:
                    st.pop();
                    triad_gen->gen_goto_inc();
                    break;

                case SemProc::GenNop:
                    st.pop();
                    triad_gen->generate_nop();
                    break;

                case SemProc::SetNextAddrToLastIf:
                    st.pop();
                    triad_gen->set_next_addr_to_last_if();
                    break;

                case SemProc::NestScope:
                    st.pop();
                    sem_tree->CompoundOpCreateScope();
                    break;

                case SemProc::GenMult:
                    st.pop();
                    triad_gen->add_operation("*");
                    triad_gen->generate_arithm_triad();
                    break;

                case SemProc::GenDiv:
                    st.pop();
                    triad_gen->add_operation("/");
                    triad_gen->generate_arithm_triad();
                    break;

                case SemProc::GenPlus:
                    st.pop();
                    triad_gen->add_operation("+");
                    triad_gen->generate_arithm_triad();
                    break;

                case SemProc::GenMinus:
                    st.pop();
                    triad_gen->add_operation("-");
                    triad_gen->generate_arithm_triad();
                    break;

                case SemProc::GenRSh:
                    st.pop();
                    triad_gen->add_operation(">>");
                    triad_gen->generate_arithm_triad();
                    break;

                case SemProc::GenLSh:
                    st.pop();
                    triad_gen->add_operation("<<");
                    triad_gen->generate_arithm_triad();
                    break;

                case SemProc::GenGT:
                    st.pop();
                    triad_gen->add_operation(">");
                    triad_gen->generate_arithm_triad();
                    break;

                case SemProc::GenLT:
                    st.pop();
                    triad_gen->add_operation("<");
                    triad_gen->generate_arithm_triad();
                    break;

                case SemProc::GenEq:
                    st.pop();
                    triad_gen->add_operation("==");
                    triad_gen->generate_arithm_triad();
                    break;

                case SemProc::GenNEq:
                    st.pop();
                    triad_gen->add_operation("!=");
                    triad_gen->generate_arithm_triad();
                    break;
            }
        }
        else {
            assert(0);
        }
    }

    sc->print_error("Job Done!!");
}
