#include "codegenerator.h"
#include <cassert>
#include <string>
#include <fstream>

using namespace std;

string Reg::getName()
{
    switch (this->name) {
    case Name::eax:
        return "eax";
        break;
    case Name::ebx:
        return "ebx";
        break;
    case Name::ecx:
        return "ecx";
        break;
    case Name::edx:
        return "edx";
        break;
    case Name::esi:
        return "esi";
        break;
    case Name::edi:
        return "edi";
        break;
    default:
        return "RegNameNotSet";
    }
    return "RegNameNotSet";
}

Reg::Reg()
    : name(Name::notset) {}

Reg::Reg(const Reg &toCopy)
{
    this->name = toCopy.name;
    this->isBusy = toCopy.isBusy;
}

Reg::Reg(Reg::Name nm)
    : name(nm) {}

Reg CodeGenerator::giveFreeReg()
{
    for (int i = 0; i < allRegs.size(); i++) {
        Reg &reg = allRegs[i];
        if (!reg.isBusy) {
            reg.isBusy = true;
            return reg;
        }
    }

    asmCode.push_back("push " + allRegs[toPush].getName());
    regStack.push(allRegs[toPush]);
    Reg reg = allRegs[toPush];
    toPush++;
    if (toPush >= allRegs.size())
        toPush = 0;

    return reg;
}

void CodeGenerator::freeReg(Reg reg)
{
    if (!regStack.empty()) {
        Reg reg = regStack.top();
        asmCode.push_back("pop " + reg.getName());
        regStack.pop();
        toPush--;
        if (toPush < 0)
            toPush = regStack.size()-1;
    }
    else {
        for (int i = 0; i < allRegs.size()-1; i++) {
            if (allRegs[i].name == reg.name) {
                allRegs[i].isBusy = false;
                break;
            }
        }
    }
}

void CodeGenerator::rec(Node *node) {
    if (node->dataType == DATA_TYPE::INT && node->lex != "") {
        node->address = "DWORD PTR[ebp-"+to_string(4*ebpOffset++)+"]";
    }

    if (node->left != nullptr)
        rec(node->left);

    if (node->right != nullptr)
        rec(node->right);
}

Node* CodeGenerator::init() {
    asmCode.clear();
    toPush = 0;
    while(!regStack.empty()) regStack.pop();

    allRegs.clear();
    allRegs.push_back(Reg(Reg::Name::eax));
    allRegs.push_back(Reg(Reg::Name::ebx));
    allRegs.push_back(Reg(Reg::Name::ecx));
    allRegs.push_back(Reg(Reg::Name::edx));
    allRegs.push_back(Reg(Reg::Name::esi));
    allRegs.push_back(Reg(Reg::Name::edi));

    ebpOffset = 1;
    int increment = 1;
    int funstart = -1;
    Node *cur = st->getRoot();
    while (cur->left != nullptr) {
        cur = cur->left;
        if (cur->dataType == DATA_TYPE::INT && cur->lex != "") {
            cur->isGlobal = true;
            asmCode.push_back(cur->lex+to_string(increment) + ": " + cur->lex);
            cur->address = cur->lex + to_string(increment++) + "[rip]";
        }

        if (cur->dataType == DATA_TYPE::FUNC) {
            funstart = cur->func_triad_idx;
            break;
        }
    }

    if (funstart >= 0) {        // function found
        rec(cur->right);        // set ebp offset and set names for local variables
    }

    return cur;
}

void CodeGenerator::generateAsm(vector<Triad> &triads, SemTree *st)
{
    this->triads = triads;
    this->st = st;
    Node *fun = init();
    if (fun->dataType != DATA_TYPE::FUNC) return;            // no functions sorry.

    asmCode.push_back(fun->lex+" proc");
    asmCode.push_back("push ebp");
    asmCode.push_back("mov ebp, esp");
    asmCode.push_back("sub esp, "+to_string((ebpOffset-1)*4));

    for (int i = fun->func_triad_idx; i < (int)triads.size(); i++) {
        Triad &curTriad = triads[i];
        if (curTriad.operation == "endp")
            break;

        if (curTriad.operation == "=") {
            string assignLine = "mov "+curTriad.op1.tree_node->address+", ";
            if (curTriad.op2.getType() == LinkToTriad) {
                assignLine += triads[curTriad.op2.triad_idx].resultReg->getName();
                freeReg(*(triads[curTriad.op2.triad_idx].resultReg));
            }
            else if (curTriad.op2.getType() == Literal) {
                assignLine += curTriad.op2.toString();
            }
            else if (curTriad.op2.getType() == LinkToTree) {
                assignLine += curTriad.op2.tree_node->address;
            }
            else throw 1;

            asmCode.push_back(assignLine);
        }
        else if (curTriad.operation == "+") {
            TriadVal &op1 = curTriad.op1, &op2 = curTriad.op2;
            if (op1.getType()==LinkToTriad && op2.getType()==LinkToTriad) {
                Reg reg1 = *triads[op1.triad_idx].resultReg;
                Reg reg2 = *triads[op2.triad_idx].resultReg;
                asmCode.push_back("add " + reg1.getName() + ", " + reg2.getName());
                this->freeReg(reg2);
                curTriad.resultReg = new Reg(reg1);
            }
            else if (op1.getType()==LinkToTriad) {
                Reg reg = *triads[op1.triad_idx].resultReg;
                asmCode.push_back("add " + reg.getName() + ", " + op2.toAsmString());
                curTriad.resultReg = new Reg(reg);
            }
            else if (op2.getType()==LinkToTriad) {
                Reg reg = *triads[op2.triad_idx].resultReg;
                asmCode.push_back("add " + reg.getName() + ", " + op1.toAsmString());
                curTriad.resultReg = new Reg(reg);
            }
            else {
                Reg reg = this->giveFreeReg();
                asmCode.push_back("mov " + reg.getName() + ", " + op1.toAsmString());
                asmCode.push_back("add " + reg.getName() + ", " + op2.toAsmString());
                curTriad.resultReg = new Reg(reg);
            }
        }
        else if (curTriad.operation == "*") {
            TriadVal &op1 = curTriad.op1, &op2 = curTriad.op2;
            if (op1.getType()==LinkToTriad && op2.getType()==LinkToTriad) {
                Reg reg1 = *triads[op1.triad_idx].resultReg;
                Reg reg2 = *triads[op2.triad_idx].resultReg;
                asmCode.push_back("imul " + reg1.getName() + ", " + reg2.getName());
                this->freeReg(reg2);
                curTriad.resultReg = new Reg(reg1);
            }
            else if (op1.getType()==LinkToTriad) {
                Reg reg = *triads[op1.triad_idx].resultReg;
                asmCode.push_back("imul " + reg.getName() + ", " + op2.toAsmString());
                curTriad.resultReg = new Reg(reg);
            }
            else if (op2.getType()==LinkToTriad) {
                Reg reg = *triads[op2.triad_idx].resultReg;
                asmCode.push_back("imul " + reg.getName() + ", " + op1.toAsmString());
                curTriad.resultReg = new Reg(reg);
            }
            else {
                Reg reg = this->giveFreeReg();
                asmCode.push_back("mov " + reg.getName() + ", " + op1.toAsmString());
                asmCode.push_back("imul " + reg.getName() + ", " + op2.toAsmString());
                curTriad.resultReg = new Reg(reg);
            }
        }
    }

    asmCode.push_back("mov esp, ebp");
    asmCode.push_back("pop ebp");
    asmCode.push_back(fun->lex + " endp");

    ofstream opf("asm.txt");
    for (int i = 0; i < (int)asmCode.size(); i++) {
        opf << asmCode[i] << endl;
    }
}
