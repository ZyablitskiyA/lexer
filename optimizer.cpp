#include "optimizer.h"
#include <cassert>
#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

//v---------------------------------------- TriadValWrapper class ------------------------------v

TriadValWrapper::TriadValWrapper(TriadVal trVal)
    : triadVal(trVal), linkTo(nullptr)
{

}

bool TriadValWrapper::isEmpty()
{
    return triadVal.isEmpty();
}

TriadValType TriadValWrapper::getType()
{
    return triadVal.getType();
}

Node* TriadValWrapper::getTreeNode()
{
    assert(this->getType() == LinkToTree);
    return triadVal.tree_node;
}

int TriadValWrapper::getTriadIdx()
{
    assert(this->getType() == LinkToTriad);
    return triadVal.triad_idx;
}

YetAnotherTriad* TriadValWrapper::getTriadPtr()
{
    assert(triadVal.getType() == LinkToTriad);
    return this->linkTo;
}

TData TriadValWrapper::getLiteral()
{
    assert(triadVal.getType() == Literal);
    return triadVal.literal;
}

void TriadValWrapper::setPtr(YetAnotherTriad *ptr)
{
    linkTo = ptr;
}

TriadVal TriadValWrapper::generateTriadVal()
{
    if (triadVal.getType() != LinkToTriad)
        return triadVal;

    assert(this->linkTo != nullptr);
    assert(this->linkTo->isIdxSet());
    return TriadVal(this->linkTo->getIdx());
}

//v---------------------------------------- YetAnotherTriad class ------------------------------v

YetAnotherTriad::YetAnotherTriad(const Triad &triad)
    : operation(triad.operation), op1(triad.op1), op2(triad.op2)
{
    myIdxInList = -1;
}

const string& YetAnotherTriad::getOperation()
{
    return this->operation;
}

TriadValWrapper& YetAnotherTriad::getOp1()
{
    return op1;
}

TriadValWrapper& YetAnotherTriad::getOp2()
{
    return op2;
}

void YetAnotherTriad::setIdx(int idx)
{
    myIdxInList = idx;
}

int YetAnotherTriad::getIdx()
{
    return myIdxInList;
}

Triad YetAnotherTriad::generateTriad()
{
    return Triad(this->operation, op1.generateTriadVal(), op2.generateTriadVal());
}

bool YetAnotherTriad::isIdxSet()
{
    return myIdxInList != -1;
}

//v---------------------------------------- Optimizer class ------------------------------v

Optimizer::Optimizer()
{

}

void Optimizer::convert_to_internal_triads(const vector<Triad> &triads)
{
    internal_triads.clear();
    for (auto it = triads.begin(); it != triads.end(); it++) {
        internal_triads.push_back(new YetAnotherTriad(*it));
    }

    for (int i = 0; i < (int)internal_triads.size(); i++) {
        TriadValWrapper &op1 = internal_triads[i]->getOp1();
        TriadValWrapper &op2 = internal_triads[i]->getOp2();

        if (op1.getType() == LinkToTriad) {
            int idx = op1.getTriadIdx();
            op1.setPtr(internal_triads[idx]);
        }

        if (op2.getType() == LinkToTriad) {
            int idx = op2.getTriadIdx();
            op2.setPtr(internal_triads[idx]);
        }
    }
}

int Optimizer::find_next_goto(int start)
{
    for (int i = start; i < (int)internal_triads.size(); i++) {
        if (internal_triads[i]->getOperation() == "GOTO") return i;
    }

    return -100500;     // to gurantee segfault
}

void Optimizer::find_loops()
{
    set_triad_indexes();

    loops.clear();
    for (int i = 0; i < (int)internal_triads.size(); i++) {
        YetAnotherTriad *cur = internal_triads[i];
        if (cur->getOperation() == "IF") {
            Loop cur_loop;

            cur_loop.end = cur->getOp2().getTriadPtr();                           // first triad after loop
            int after_goto_idx = cur_loop.end->getIdx();
            YetAnotherTriad *goto_triad = internal_triads[after_goto_idx-1];
            int inc_idx = goto_triad->getOp1().getTriadPtr()->getIdx();           // idx of the first triad of increment

            YetAnotherTriad *inc_goto = internal_triads[find_next_goto(inc_idx)];
            cur_loop.begin = inc_goto->getOp1().getTriadPtr();                    // first triad of loop cond

            loops.push_back(cur_loop);
        }
    }
}

bool is_can_be_hoisted(YetAnotherTriad* yat)
{
    string o = yat->getOperation();
    if (o == "*" || o == "/" || o == "DEREF" || o == "+" || o == "-" || o == "<<" ||
           o == ">>" || o == ">" || o == "<" || o == "==" || o == "!=") return true;
    return false;
}

void Optimizer::hoist_invariants(Loop loop)
{
    this->set_triad_indexes();

    set<Node*> non_invariant_vars;
    for (int i = loop.begin->getIdx(); i < loop.end->getIdx(); i++) {
        YetAnotherTriad *triad = internal_triads[i];

        if (internal_triads[i+1]->getOperation() == "IF")           // we cannot hoist since flags must be set.
            continue;

        if (triad->getOperation() == "=") {                         // find assignment
            if (triad->getOp1().getType() == LinkToTree)                 // find assignment to a variable (not to array)
                non_invariant_vars.insert(triad->getOp1().getTreeNode());
            else {                                                  // find assignment to an array
                assert(triad->getOp1().getType() == LinkToTriad);
                assert(triad->getOp1().getTriadPtr()->getOperation() == "DEREF");
                non_invariant_vars.insert(triad->getOp1().getTriadPtr()->getOp1().getTreeNode());    // array is non-invariant
            }
        }
        else if (triad->getOperation() == "CALL") {                 // met a function call. Mark all globals as non-invariant
            // traverse semtree and mark them all!
        }
    }

    std::vector<YetAnotherTriad*> cur_inv;                             // invariants in this loop

    for (int i = loop.begin->getIdx(); i < loop.end->getIdx(); i++) {
        YetAnotherTriad *triad = internal_triads[i];

        if (!is_can_be_hoisted(triad))          // check triad operation.
            continue;

        bool inv = true;
        if (!triad->getOp1().isEmpty()) {
            if (triad->getOp1().getType() == LinkToTree) {              // if we linkTo non-invariant var or array
                if (non_invariant_vars.count(triad->getOp1().getTreeNode()) != 0)
                    inv = false;
            }
            else if (triad->getOp1().getType() == LinkToTriad) {        // or to non_invariant triad
                if (invariant.count(triad->getOp1().getTriadPtr()) == 0)
                    inv = false;
            }
        }

        if (!triad->getOp2().isEmpty()) {
            if (triad->getOp2().getType() == LinkToTree) {              // if we linkTo non-invariant var or array
                if (non_invariant_vars.count(triad->getOp2().getTreeNode()) != 0)
                    inv = false;
            }
            else if (triad->getOp2().getType() == LinkToTriad) {        // or to non_invariant triad
                if (invariant.count(triad->getOp2().getTriadPtr()) == 0)
                    inv = false;
            }
        }

        if (inv) {
            invariant.insert(triad);                                    // add to global (whole program) invariants
            cur_inv.push_back(triad);
        }
    }
    // create local invariant set and megrge them. 'this iteration set' is needed for insertion of his elems to code.

    //*
    for (auto it = invariant.begin(); it != invariant.end(); it++) {
        internal_triads.erase(std::remove_if(internal_triads.begin()+loop.begin->getIdx(),
                                             internal_triads.end(),
                                             [this](YetAnotherTriad* ptr) -> bool {
                                  return invariant.count(ptr) > 0;
                              }), internal_triads.end());
    }//*/

    // v-----------debug----------v
/*
    ofstream invariant_log("invariant.txt");
    vector<Triad> vec = getTriads();
    for (auto it = invariant.begin(); it != invariant.end(); it++)
        vec[(*it)->getIdx()].operation = "inv!!!!!!!!!!! " + vec[(*it)->getIdx()].operation;
    for (int i = 0; i < vec.size(); i++) {
        invariant_log << vec[i].operation << " " << vec[i].op1.toString() << " " << vec[i].op2.toString() << endl;
    }
//*/
    internal_triads.insert(internal_triads.begin()+loop.begin->getIdx(), cur_inv.begin(), cur_inv.end());
}

vector<Triad> Optimizer::optimize_loops(const vector<Triad> &triads)
{
    convert_to_internal_triads(triads);
    find_loops();

    /*
    cout << "v---------- loops--------v" << endl;
    for (int i = 0; i < (int)loops.size(); i++) {
        cout << loops[i].begin->getIdx()+1 << ' ' << loops[i].end->getIdx()+1 << endl;
    }
    cout << "^---------- loops--------^" << endl;
    */
    for (int i = 0; i < (int)loops.size(); i++) {
        hoist_invariants(loops[i]);
    }


    return getTriads();
}

void Optimizer::set_triad_indexes()
{
    for (int i = 0; i < (int)internal_triads.size(); i++)
        internal_triads[i]->setIdx(i);
}

vector<Triad> Optimizer::getTriads()
{
    set_triad_indexes();

    vector<Triad> res;
    for(int i = 0; i < (int)internal_triads.size(); i++) {
        res.push_back(internal_triads[i]->generateTriad());
    }

    return res;
}
