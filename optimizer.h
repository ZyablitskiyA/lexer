#ifndef OPTIMIZER_H
#define OPTIMIZER_H

#include "triadgenerator.h"
#include <list>
#include <set>

class YetAnotherTriad;

class TriadValWrapper {
public:
    // Copy the triad to its internals
    TriadValWrapper(TriadVal trVal);
    bool isEmpty();
    TriadValType getType();

    Node* getTreeNode();
    int getTriadIdx();
    YetAnotherTriad *getTriadPtr();
    TData getLiteral();

    void setPtr(YetAnotherTriad *ptr);
    TriadVal generateTriadVal();

private:
    TriadVal triadVal;
    YetAnotherTriad *linkTo;
};

class YetAnotherTriad {
public:
    YetAnotherTriad(const Triad &triad);
    const string& getOperation();
    TriadValWrapper& getOp1();
    TriadValWrapper& getOp2();

    void setIdx(int idx);
    int getIdx();

    Triad generateTriad();

    bool isIdxSet();

private:
    string operation;
    TriadValWrapper op1;
    TriadValWrapper op2;
    int myIdxInList;
};

class Optimizer
{
public:
    Optimizer();

    vector<Triad> optimize_loops(const vector<Triad> &triads);
    vector<Triad> getTriads();

private:
    struct Loop {
    public:
        YetAnotherTriad *begin;         // points to first triad of B in for(A;B;C) D;
        YetAnotherTriad *end;           // points to the triad next to D
    };

    void find_loops();
    void hoist_invariants(Loop loop);
    void convert_to_internal_triads(const vector<Triad> &triads);
    //find_loop_from
    std::vector<YetAnotherTriad*> internal_triads;
    void set_triad_indexes();
    int find_next_goto(int start);

    std::vector<Loop> loops;
    std::set<YetAnotherTriad*> invariant;
};

#endif // OPTIMIZER_H
