#ifndef LEXER_DEFS_H
#define LEXER_DEFS_H

#define MAX_TEXT_LEN 10000
#define MAX_LEX_LEN   20            // warning it was 10 test this

#include <string>

namespace lexer {

    enum TokenType
    {
        TFor, TAssign, TSc, TComma, TInt, TLL, TVoid,

        TIdent, TDecLiter, THexLiter,

        TLParen, TRParen, TLBrace, TRBrace, TLSqB, TRSqB,

        TLT, TLE, TGT, TGE, TEq, TNEq,

        TLSh, TRSh, TPlus, TMinus, TMult, TDiv,

        TEnd, TErr
    };

    TokenType get_key_word_type(const std::string &lexeme);
    //void print_error(const std::string &message);
}

#endif // LEXER_DEFS_H

