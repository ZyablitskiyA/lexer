#ifndef PARSER_H
#define PARSER_H
#include "scanner.h"
#include "semtree.h"

class Parser
{
private:
    lexer::Scanner *scanner;
public:
    Parser(lexer::Scanner *sc);
    SemTree st;
    bool flInt;     // interpret flag

    void P();       // Program
    void D();       // Data definition (int a = 5;)
    void G();       // Function definition
    void Z(DATA_TYPE dt);       // Data definition sequance (a, a = 5, a[7] = 5)
    void X();       // Compound operator
    void Q();       // sequance of operators and data definitions;
    void O();       // Operator
    void U();       // for (a = V; V; a = V) O

    TData V();       // M != M
    TData M();       // S >= S
    TData S();       // A >> A
    TData A();       // B + B
    TData B();       // E * E
    TData E();       // -c, (V),  a[V],  a
};

#endif // PARSER_H
