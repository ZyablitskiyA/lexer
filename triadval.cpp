#include "triadval.h"

using namespace std;

TriadVal::TriadVal()
{
    is_empty = true;
}

TriadVal::TriadVal(const TData &_literal)
{
    is_empty = true;
    set(_literal);
}

TriadVal::TriadVal(int _triad_idx)
{
    is_empty = true;
    set(_triad_idx);
}

TriadVal::TriadVal(Node *_tree_node)
{
    is_empty = true;
    set(_tree_node);
}

bool TriadVal::isEmpty() const
{
    return is_empty;
}

string TriadVal::toString() const
{
    if (this->isEmpty())
        return "";         // change it to empty str;

    switch (val_type) {
        case Literal:
            if (literal.dt == INT)
                return to_string(literal.val.dataAsInt);
            else if (literal.dt ==  INT64)
                return to_string(literal.val.dataAsLL);
            else throw 1;
        break;

        case LinkToTriad:
            return "(" + to_string(triad_idx+1) + ")";
        break;

        case LinkToTree:
            return tree_node->lex;
        break;
    }

    return "toString() error";
}

TriadValType TriadVal::getType()
{
    if (isEmpty()) return Empty;
    return val_type;
}

void TriadVal::set(const TData &_literal)
{
    if (!this->isEmpty()) throw 1;

    is_empty = false;
    val_type = Literal;
    literal = _literal;
}

void TriadVal::set(int _triad_idx)
{
    if (!this->isEmpty()) throw 1;

    is_empty = false;
    val_type = LinkToTriad;
    triad_idx = _triad_idx;
}

void TriadVal::set(Node *_tree_node)
{
    if (!this->isEmpty()) throw 1;

    is_empty = false;
    val_type = LinkToTree;
    tree_node = _tree_node;
}

string TriadVal::toAsmString()
{
    if (this->getType()==LinkToTree) {
        return this->tree_node->address;
    }
    else if (this->getType()==Literal) {
        return to_string(this->literal.val.dataAsInt);
    }
    else throw 1;
}
