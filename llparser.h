#ifndef LLPARSER_H
#define LLPARSER_H
#include <stack>
#include "scanner.h"
#include "semtree.h"
#include "triadgenerator.h"

using lexer::Scanner;

enum NeTerm {
    P, D, Z, C, G, V, V1, M, M1, S, S1, A, A1, B, B1, E, E1, O, K, U, Q
};

enum SemProc {
    ProcessLiteral, GenMult, GenDiv, GenPlus, GenMinus, GenRSh, GenLSh, GenGT, GenLT, GenNEq, GenEq, UnaryMinus, RememberCondAddress,
    IncludeVar, AddArrDim, LastVarToOperands, GenAssignment, LeaveOneScope, GenerateProc, SaveFunctionIdx, GenerateEndp, FindInTree,
    GenArrDerefTriad, ForceArithmFlags, PushOpPrev_token_tree, GenEmptyIf, GenGotoCond, RememberIncAddress, GenGotoInc, GenNop,
    SetNextAddrToLastIf, NestScope
};

enum SymbolType {
    Term, Neterm, Sem
};

struct Symbol
{
    Symbol(lexer::TokenType _t);
    Symbol(NeTerm _nt);
    Symbol(SemProc _sem);

    SymbolType type;
    union {
        lexer::TokenType t;
        NeTerm nt;
        SemProc sem;
    };

    SymbolType getType();
};

class LlParser
{
public:         // private
    std::stack<Symbol> st;
    Scanner *sc;
    SemTree *sem_tree;
    TriadGenerator *triad_gen;

public:
    LlParser(lexer::Scanner *_sc);

    void doYourJob();
};

#endif // LLPARSER_H
