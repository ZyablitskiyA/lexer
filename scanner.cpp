#include "scanner.h"
#include "cstdlib"
#include "iostream"
// 9for problem?

using namespace lexer;
using namespace std;

Scanner::Scanner(const std::string &file_name)
{
    source.clear();
    this->pos = 0;

    FILE *in = fopen(file_name.c_str(), "r");
    if (in == NULL) print_error("file not opened");
    char ch;
    while(true) {
        fscanf(in, "%c", &ch);

        if (feof(in)) break;

        if (source.size() >= MAX_TEXT_LEN) {
            print_error("too long file");
            break;
        }

        source.push_back(ch);
    }

    source.push_back('\0');

    //for (int i = 0; i < (int)source.size(); i++) { \
        std::cerr << source[i]; \
    }
}

inline bool is_space(char ch)
{
    return (ch == ' ' || ch == '\n' || ch == '\t');
}

inline bool is_letter(char ch)
{
    return (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z' || ch == '_');
}

inline bool is_digit(char ch)
{
    return (ch >= '0' && ch <= '9');
}

inline bool is_hex_digit(char ch)
{
    return (ch >= '0' && ch <= '9' || ch >= 'a' && ch <= 'f' || ch >= 'A' && ch <= 'F');
}

Token Scanner::nextToken()
{
    string lexeme;

    start:

    // skip whitespaces
    while (is_space(source[pos])) pos++;

    // skip comment
    if (source[pos] == '/' && source[pos+1] == '/') {
        pos += 2;
        while (pos < source.size() && source[pos] != '\n') pos++;
        goto start;
    }

    // End of file reached
    if (source[pos] == '\0') {
        return Token("#", TokenType::TEnd);
    }

    // Identificator or key word
    if (is_letter(source[pos])) {
        while (is_letter(source[pos]) || is_digit(source[pos])) {
            if (lexeme.size() < MAX_LEX_LEN) lexeme.push_back(source[pos++]);
            else pos++;
        }

        return Token(lexeme, get_key_word_type(lexeme));
    }
    else if (source[pos] == '0') {
        lexeme.push_back(source[pos++]);            // read '0'
        if ((source[pos] == 'x' || source[pos] == 'X') && is_hex_digit(source[pos+1])) {
            lexeme.push_back(source[pos++]);        // read 'x'
            goto read_hex;                          // read remainder of the hex number
        }
        else {
            goto read_dec;                          // read remainder of the decimal (in fact: octal) number
        }
    }
    else if (is_digit(source[pos])) {
        lexeme.push_back(source[pos++]);

        goto read_dec;
    }
    else if (source[pos] == '<') {
        lexeme.push_back(source[pos++]);

        if (source[pos] == '=') {
            lexeme.push_back(source[pos++]);
            return Token(lexeme, TokenType::TLE);
        }
        else if (source[pos] == '<') {
            lexeme.push_back(source[pos++]);
            return Token(lexeme, TokenType::TLSh);
        }
        else {
            return Token(lexeme, TokenType::TLT);
        }
    }
    else if (source[pos] == '>') {
        lexeme.push_back(source[pos++]);

        if (source[pos] == '=') {
            lexeme.push_back(source[pos++]);
            return Token(lexeme, TokenType::TGE);
        }
        else if (source[pos] == '>') {
            lexeme.push_back(source[pos++]);
            return Token(lexeme, TokenType::TRSh);
        }
        else {
            return Token(lexeme, TokenType::TGT);
        }
    }
    else if (source[pos] == '=') {
        lexeme.push_back(source[pos++]);

        if (source[pos] == '=') {
            lexeme.push_back(source[pos++]);
            return Token(lexeme, TokenType::TEq);
        }
        else {
            return Token(lexeme, TokenType::TAssign);
        }
    }
    else if (source[pos] == '!') {
        lexeme.push_back(source[pos++]);
        if (source[pos] == '=') {
            lexeme.push_back(source[pos++]);
            return Token(lexeme, TokenType::TNEq);
        }
        else {
            return Token(lexeme, TokenType::TErr);      // single '!' is an error
        }
    }
    else if (source[pos] == '[' || source[pos] == ']' || source[pos] == '(' || source[pos] == ')' || \
             source[pos] == '{' || source[pos] == '}' || source[pos] == ',' || source[pos] == ';' || \
             source[pos] == '+' || source[pos] == '-' || source[pos] == '/' || source[pos] == '*')
    {
        lexeme.push_back(source[pos++]);
        switch (source[pos-1]) {
            case '[': return Token(lexeme, TokenType::TLSqB);
            case ']': return Token(lexeme, TokenType::TRSqB);
            case '(': return Token(lexeme, TokenType::TLParen);
            case ')': return Token(lexeme, TokenType::TRParen);
            case '{': return Token(lexeme, TokenType::TLBrace);
            case '}': return Token(lexeme, TokenType::TRBrace);
            case ',': return Token(lexeme, TokenType::TComma);
            case ';': return Token(lexeme, TokenType::TSc);
            case '+': return Token(lexeme, TokenType::TPlus);
            case '-': return Token(lexeme, TokenType::TMinus);
            case '*': return Token(lexeme, TokenType::TMult);
            case '/': return Token(lexeme, TokenType::TDiv);
        }
    }
    else {
        lexeme.push_back(source[pos++]);            // charachter that no one lexeme start with
        return Token(lexeme, TokenType::TErr);
    }

    // Read part of a hex literal after "0x".
    read_hex:
    while (is_hex_digit(source[pos])) {
        if (lexeme.size() < MAX_LEX_LEN) lexeme.push_back(source[pos++]);
        else pos++;
    }

    return Token(lexeme, TokenType::THexLiter);

    read_dec:
    while (is_digit(source[pos])) {
        if (lexeme.size() < MAX_LEX_LEN) lexeme.push_back(source[pos++]);
        else pos++;
    }

    return Token(lexeme, TokenType::TDecLiter);



    must_be_unreachable:
    std::cerr << "end of nextToken() reached" << endl;
    return Token("", TokenType::TEnd);
}

int Scanner::getPos()
{
    return this->pos;
}

void Scanner::setPos(int i)
{
    this->pos = i;
}

Token Scanner::peekToken()
{
    int pos = this->getPos();
    Token t = this->nextToken();
    this->setPos(pos);

    return t;
}

void Scanner::print_error(const string &message)
{
    cerr << message << endl;
}

void Scanner::print_error(const string &massage, Token t)
{
    print_error(massage, t.lexeme());
}

void Scanner::print_error(const string &massage, const string &info)
{
    cerr << "At symbol #" << this->getPos() << " Expected: " << massage << " Got: " << info << endl;
    cerr << endl << "Source code: " << endl << "-----------" << endl << endl;
    int n = this->getPos();
    for (int i = 0; i < n; i++) {
        cout << source[i];
    }
    cout << endl << endl << "-----------" << endl;

    exit(0);
}
