#ifndef SCANNER_H
#define SCANNER_H

#include <string>
#include "lexer_defs.h"
#include "token.h"

namespace lexer {

    class Scanner
    {
    private:
        std::string source;
        int pos;

    public:
        Scanner(const std::string &file_name);

        Token nextToken();

        int getPos();
        void setPos(int i);
        Token peekToken();
        void print_error(const std::string &message);
        void print_error(const std::string &message, Token t);
        void print_error(const std::string &massage, const std::string &info);
    };

}

#endif // SCANNER_H
