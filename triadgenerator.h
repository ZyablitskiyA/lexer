#ifndef TRIADGENERATOR_H
#define TRIADGENERATOR_H

#include <stack>
#include <vector>
#include <string>
#include <triadval.h>

using std::stack;
using std::vector;
using std::string;

class Reg;

struct Triad {
    string operation;
    TriadVal op1;
    TriadVal op2;

    Triad(const string &_operation, const TriadVal &_op1, const TriadVal &_op2);

    Reg *resultReg;
};


class TriadGenerator
{
public:
    TriadGenerator();

    void add_operand(const TriadVal &operand);
    void add_operation(const string &op);
    void apply_unary_minus();
    void generate_arithm_triad();
    void generate_assignment();

    void gen_empty_if_and_save_addr();

    void add_proc(Node* func);
    void add_ret_endp();

    int get_last_triad_idx();
    void printTriads(const string &file_name);
    static void printTriads(const vector<Triad> &triads, const string &file_name);
    void remember_cond_address();
    //void remember_if_address();
    void remember_inc_address();

    void add_array(Node * arrayInTree);
    void gen_arr_deref();
    void pop_operand();
    void gen_goto_cond();
    void gen_goto_inc();
    void generate_nop();
    void set_next_addr_to_last_if();            // read the code! its tricky.

    void gen_function_call(Node * node);
    vector<Triad> getTriads();

private:
    stack<int> for_cond_address;        // Index of the triad block where loop condition is checked.

    /* Index of the 'if' triad which directs the execution to the loop body or leaves the loop
     * depending on the last loop condition check
     */
    stack<int> for_if_address;
    stack<int> for_inc_address;

    // It is needed for nested array indexing a[b[c[d[e[0]]]]]
    stack<Node*> arrays;

    stack<TriadVal> operands;
    stack<string> operations;
    vector<Triad> triads;
};

#endif // TRIADGENERATOR_H
