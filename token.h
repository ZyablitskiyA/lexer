#ifndef TOKEN_H
#define TOKEN_H

#include <string>
#include "lexer_defs.h"

namespace lexer {

    class Token {
    public:
        Token();
        Token(const std::string &s, TokenType t);

        const std::string& lexeme() const;
        TokenType type() const;

    private:
        std::string _lexeme;
        TokenType _type;
    };
}

#endif // TOKEN_H
