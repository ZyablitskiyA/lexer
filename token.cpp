#include "token.h"

using namespace std;
using namespace lexer;

Token::Token(const string &s, TokenType t) : _lexeme(s), _type(t)
{}

Token::Token() : _lexeme(""), _type(TokenType::TErr)
{}

const string& Token::lexeme() const
{
    return _lexeme;
}

TokenType Token::type() const
{
    return _type;
}
